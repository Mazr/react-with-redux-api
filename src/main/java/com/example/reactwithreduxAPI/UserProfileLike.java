package com.example.reactwithreduxAPI;

public class UserProfileLike {
    private long userProfileId;
    private String userName;

    public UserProfileLike() { }

    public UserProfileLike(long userProfileId, String userName) {
        this.userProfileId = userProfileId;
        this.userName = userName;
    }

    public long getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(long userProfileId) {
        this.userProfileId = userProfileId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
